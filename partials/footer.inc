


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>

<script src="<?php echo $url ?>/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo $url ?>/js/ie10-viewport-bug-workaround.js"></script>
<script src="<?php echo $url ?>/js/offcanvas.js"></script>
<script>
    $(function () {
        $(document).on('click', '.delete-post', function () {
            var delBtn = $(this);
            if (confirm('Are you sure?!')) {
                window.location.href = 'delete.php?id='+ delBtn.attr('data-id');
//                console.log('dddddv ',delBtn.attr('data-id'))
            }
        })
    })
</script>
</body>
</html>