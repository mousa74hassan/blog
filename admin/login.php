
<?php
//session_start();

require '../controllers/User.php';

if (isset($_POST['login'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $user = new User();

    if($user->ckAdmin($email, md5($password))) {
        header('Location: index.php');
    } else {
        echo '<script>alert("Email or Password is incorrect!")</script>>';
    }
}

?>

<?php  include '../partials/header-re.inc'; ?>

<div class="container">
    <form class="form-signin" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" name="login">
        <h2 class="form-signin-heading">Admin Area</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" name="login" type="submit">Sign in</button>
        <br>
    </form>

</div> <!-- /container -->



<?php  include '../partials/footer.inc'; ?>
