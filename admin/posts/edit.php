<?php
include '../partials/header.inc';
require '../../controllers/Post.php';
$post = new Post;
$curr_post = $post->getPost($_GET['id']);
$categories = $post->getAllCats();

if (isset($_POST['update'])) {
    $post->title = $_POST['title'];
    $post->content = $_POST['content'];
    $post->category_id = $_POST['category_id'];
    $post->fav = 1;
    $image = $curr_post[0]['img'];
    if (!empty($_FILES['image']['name'])) {
        //unlink('../images/'.$curr_post[0]['img']);
        $target_dir = "../images/";
        $image = basename($_FILES["image"]["name"]);
        $target_file = $target_dir . $image;
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
    }
    $post->img = $image;
    $post->update($_GET['id']);
    header("Location: view.php");
}
?>

<div id="wrapper">

    <!-- Navigation -->

    <!--nav-->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header"> <?php echo strtoupper("welcome"." ".htmlentities($_SESSION['user_name']));?></h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit User</div>
                    <div class="panel-body">
                        <div class="row">
                            <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$_GET['id']?>" enctype="multipart/form-data">
                                <div class="col-lg-10">


                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Title<span id="" style="font-size:11px;color:red">*</span>	</label>
                                        </div>
                                        <div class="col-lg-6">

                                            <input class="form-control" name="title" value="<?php echo $curr_post[0]['title'] ?>" placeholder="Title .." id="cshort" required="required">
                                            <span id="course-availability-status" style="font-size:12px;"></span>				</div>

                                    </div>

                                    <br><br>

                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Category<span id="" style="font-size:11px;color:red">*</span>	</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="category_id" id="category_id" required>
                                                <?php foreach($categories as $category) {
                                                    if ($curr_post[0]['category_id'] == $category['id']) {
                                                    echo "<option value='".$category['id']."' selected>".$category['name']."</option>"; continue;
                                                    } ?>
                                                    <option value="<?php echo $category['id'] ?>"><?php echo $category['name']?></option>
                                                <?php } ?>
                                            </select>
                                            <span id="course-availability-status" style="font-size:12px;"></span>				</div>

                                    </div>

                                    <br><br>

                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Image</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="file" class="form-control" name="image" id="image" >
                                            <span id="course-status" style="font-size:12px;"></span>				</div>
                                    </div>

                                    <br><br>

                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Content<span id="" style="font-size:11px;color:red">*</span></label>
                                        </div>
                                        <div class="col-lg-6">
                                            <textarea name="content" id="content" placeholder="Type a content" cols="30" rows="10" class="form-control" required><?php echo $curr_post[0]['content'] ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <br><br>

                                <div class="form-group">
                                    <div class="col-lg-4">

                                    </div>
                                    <div class="col-lg-6"><br><br>
                                        <input type="submit" class="btn btn-primary" name="update" value="Update Post">
                                    </div>

                                </div>
                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<?php include '../partials/footer.inc'?>
