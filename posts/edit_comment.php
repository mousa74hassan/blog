<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 27/05/2019
 * Time: 02:01 PM
 */

include '../partials/header.inc';
require_once '../controllers/DBConnect.php';
require '../controllers/Comment.php';

$comment = new Comment();
$comm = $comment->getComment($_GET['id']);

if (isset($_POST['update'])) {
$comment->content = $_POST['comment'];
$comment->update($_GET['id']);
header('Location: view.php?id='.$_GET['post_id']);
}
?>

<div class="container">
    <div class="jumbotron">
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$_GET['id'].'&post_id='.$_GET['post_id'] ?>" method="post">
            <div class="form-group">
                <input type="text" name="comment" class="form-control" value="<?php echo $comm[0]['content']?>" placeholder="Write a comment ..." required>
            </div>
            <div class="form-group">
                <button type="submit" name="update" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>
</div>


<?php include '../partials/footer.inc'; ?>
