<?php

require 'controllers/User.php';

if (count($_POST) > 0) {

    $name = $_POST['name'];
    $password = $_POST['password'];
    $email = $_POST['email'];

    $user = new User;
    $user->name = $name;
    $user->email = $email;
    $user->password = md5($password);
    $user->status = 0;
    $user->create();
    $_SESSION['success'] = 'New user was created successfully, Now Login';
    header('location: login.php');
}
?>

<?php  include 'partials/header-re.inc'; ?>

<div class="container">
    <form class="form-signin" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" method="post">
        <h2 class="form-signin-heading">Create new Account</h2>

        <label for="name" class="sr-only">Email address</label>
        <input type="name" id="name" name="name" class="form-control" placeholder="Full Name" required autofocus>

        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>

        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Create</button>
    </form>

</div> <!-- /container -->



<?php  include 'partials/footer.inc'; ?>
