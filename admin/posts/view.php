<?php
include '../partials/header.inc';
require '../../controllers/Post.php';
$post = new Post;
$posts = $post->getAllPosts();
$n=0;
?>

<div id="wrapper">

    <!-- Navigation -->

    <!--nav-->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header"> <?php echo strtoupper("welcome"." ".htmlentities($_SESSION['user_name']));?></h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Users</div>
                    <div class="panel-body">
                        <div class="row">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                <th>#</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Content</th>
                                <th>User</th>
                                <th>Distinct</th>
                                <th>Actions</th>
                                </thead>
                                <tbody>
                                <?php foreach($posts as $pos) { ++$n; ?>
                                <tr>
                                    <td><?php echo $n ?></td>
                                    <td><?php echo $pos['title'] ?></td>
                                    <td><?php echo $post->postCategory($pos['category_id'])[0]['name'] ?></td>
                                    <td><?php echo $pos['content'] ?></td>
                                    <td><?php echo $post->postUser($pos['user_id'])[0]['name'] ?></td>
                                    <td><?php echo $pos['fav'] == 1 ? 'Yes' : 'No' ?></td>
                                    <td><?php if ($_SESSION['user_id'] == $pos['user_id']) { ?>
                                        <a href="edit.php?id=<?php echo $pos['id']?>" class="btn btn-default">Edit</a>
                                        <?php } ?>
                                        <a href="delete.php?id=<?php echo $pos['id']?>" class="btn btn-danger">Delete</a></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<?php include '../partials/footer.inc'?>
