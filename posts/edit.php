<?php
include '../partials/header.inc';

require '../controllers/Post.php';
$post = new Post;
$categories = $post->getAllCats();
$myPost = $post->getPost($_GET['id']);


if (isset($_POST['update'])) {
    $post->title = $_POST['title'];
    $post->content = $_POST['content'];
    $post->category_id = $_POST['category_id'];
    $image = $myPost[0]['img'];
    if (!empty($_FILES['image']['name'])) {
        //unlink('../images/'.$myPost[0]['img']);
        $target_dir = "../images/";
        $image = basename($_FILES["image"]["name"]);
        $target_file = $target_dir . $image;
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
    }
    $post->img = $image;
    $post->update($_GET['id']);
    header("Location: view.php?id=".$_GET['id']);
}
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 27/05/2019
 * Time: 10:15 AM
 */
?>

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
            <p class="pull-right visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
            </p>
            <div class="jumbotron">
                <h1>Edit Post</h1>
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$_GET['id'] ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" value="<?php echo $myPost[0]['title'] ?>" class="form-control" placeholder="Title" required>
                    </div>
                    <div class="form-group">
                        <label for="category_id">Title</label>
                        <select name="category_id" id="category_id" class="form-control">
                            <?php foreach ($categories as $category) {
                                if ($myPost[0]['category_id'] == $category['id']) {
                                    echo "<option value='".$category['id']."' selected>".$category['name']."</option>"; continue;
                                }
                                echo "<option value='".$category['id']."'>".$category['name']."</option>";
                            } ?>

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="img">Image</label>
                        <input type="file" name="image" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea name="content" id="content" cols="30" rows="10" class="form-control"><?php echo $myPost[0]['content'] ?></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="update" class="btn btn-primary" value="Update">
                    </div>
                </form>
            </div>
        </div><!--/.col-xs-12.col-sm-9-->


    </div><!--/row-->

    <hr>

    <footer>
        <p>&copy; <?php echo date('Y')?> Mousa, Inc.</p>
    </footer>

</div><!--/.container-->

<?php include '../partials/footer.inc'; ?>
