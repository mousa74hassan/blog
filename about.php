<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 27/05/2019
 * Time: 01:27 PM
 */
include 'partials/header.inc';
require 'controllers/Setting.php';
$setting = new Setting();
?>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h2>About our Blog</h2>
            <p><?php echo $setting->getAbout()?></p>
        </div>
    </div>
</div>


<?php include 'partials/footer.inc'; ?>
