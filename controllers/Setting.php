<?php
session_start();
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 27/05/2019
 * Time: 01:33 PM
 */
require_once 'DBConnect.php';

class Setting
{
    public $key;
    public $value;
    private $db;

    public function __construct()
    {
        $this->db = new DBConnect();
    }

    public function getAbout()
    {
        return ($this->db->selectAll("SELECT * FROM settings WHERE setting_key='about'"))[0]['setting_value'];
    }

    public function getContact()
    {
        return ($this->db->selectAll("SELECT * FROM settings WHERE setting_key='contact'"))[0]['setting_value'];
    }

    public function update()
    {
        $this->db->insertDB('UPDATE settings SET setting_value=? WHERE setting_key=?', [$this->value, $this->key]);
    }
}