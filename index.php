<?php
require 'controllers/Post.php';
require 'checkLogin.php';
checkLogin();

$post = new Post();
$myPosts = $post->getMyPosts();
$posts = $post->index();
$favPosts = $post->favPosts();

?>


<?php  include 'partials/header.inc'; ?>

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
            <p class="pull-right visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
            </p>
            <div class="jumbotron">
                <?php foreach ($favPosts as $favPost) { ?>
                <h1><?php echo $favPost['title']?></h1>
                <p><?php echo substr($favPost['content'], 0, 250).'...'?></p>
                    <a href="posts/view.php?id=<?php echo $favPost['id']?>" class="btn btn-primary">View Details</a>
                <?php } ?>
            </div>
            <div class="row">
                <?php foreach ($posts as $s_post) { ?>
                <div class="col-xs-6 col-lg-4">
                    <h2 style="height: 60px"><?php echo substr($s_post['title'],0,50)?></h2>
                    <p style="height: 80px"><?php echo substr($s_post['content'], 0, 150).'...' ?></p>
                    <p><a class="btn btn-default" href="posts/view.php?id=<?php echo $s_post['id']?>" role="button">View details &raquo;</a></p>
                </div><!--/.col-xs-6.col-lg-4-->
                <?php } ?>
            </div><!--/row-->
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
            <div class="list-group">
                <a href="posts/create.php" class="list-group-item active">Add new post</a>
                <?php foreach ($myPosts as $myPost) {
                    echo '<a href="posts/view.php?id='.$myPost['id'].'" class="list-group-item">'.$myPost['title'].'</a>';
                } ?>


            </div>
        </div><!--/.sidebar-offcanvas-->
    </div><!--/row-->

    <hr>

    <footer>
        <p>&copy; <?php echo date('Y')?> Mousa, Inc.</p>
    </footer>

</div><!--/.container-->

<?php  include 'partials/footer.inc'; ?>
