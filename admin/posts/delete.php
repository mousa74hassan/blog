<?php
/**
 * Created by PhpStorm.
 * User: mousa
 * Date: 30/05/19
 * Time: 05:55 م
 */

require '../../controllers/Post.php';
$url = 'http://'.$_SERVER['HTTP_HOST'].'/blog';

$post = new Post;
if (isset($_GET['type'])) {
    $post->delete($_GET['id']);
    header("Location: view.php");
}
?>
<script>
    if (confirm('Are you sure?')) {
        window.location.href = "<?php echo $url.'/admin/posts/delete.php?id='.$_GET['id'].'&type=sure'?>"
    } else {
        window.location.href = "<?php echo $url.'/admin/posts/view.php'?>"
    }
</script>
