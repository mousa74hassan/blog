
<?php
//session_start();

require 'controllers/User.php';

if (isset($_POST['login'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $user = new User();
    
    if($user->chkLogin($email, md5($password))) {
        header('Location: index.php');
    } else {
        $_SESSION['failed'] = 'Email or Password is incorrect!';
        header('location: login.php');
    }
}

?>

<?php  include 'partials/header-re.inc'; ?>

<div class="container">
    <?php
    if (isset($_SESSION['success'])) {
        echo '<div class="alert alert-success"><p>'.$_SESSION['success'].'</p></div>';
        session_unset();
    }
    if (isset($_SESSION['failed'])) {
        echo '<div class="alert alert-danger"><p>'.$_SESSION['failed'].'</p></div>';
//        session_unset();
    }
    ?>
    <form class="form-signin" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" name="login">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="remember-me"> Remember me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" name="login" type="submit">Sign in</button>
        <br>
        <a href="register.php">Create new Account?</a>
    </form>

</div> <!-- /container -->



<?php  include 'partials/footer.inc'; ?>
