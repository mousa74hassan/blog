<?php
/**
 * Created by PhpStorm.
 * User: mousa
 * Date: 30/05/19
 * Time: 05:55 م
 */

require '../../controllers/User.php';
$url = 'http://'.$_SERVER['HTTP_HOST'].'/blog';

$user = new User;
if (isset($_GET['type'])) {
    $user->delete($_GET['id']);
    header("Location: view.php");
}
?>
<script>
    if (confirm('Are you sure?')) {
        window.location.href = "<?php echo $url.'/admin/users/delete.php?id='.$_GET['id'].'&type=sure'?>"
    } else {
        window.location.href = "<?php echo $url.'/admin/users/view.php'?>"
    }
</script>
