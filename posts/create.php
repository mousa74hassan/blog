
<?php include '../partials/header.inc';

require '../controllers/Post.php';
$post = new Post;
$categories = $post->getAllCats();

if (isset($_POST['create'])) {
        $title = $_POST['title'];
$img = $_FILES['image']['name'];
$content = $_POST['content'];
$category_id = $_POST['category_id'];
$image = '';

if (empty($title) || empty($content)) {
    echo "<script>alert('Please Fill required attributes')</script>";
} else {
    $post->title = $title;
    $post->content = $content;
    $post->category_id = $category_id;
    $post->user_id = $_SESSION['user_id'];
    if (empty($img)) {
        $image = 'default.png';
    } else {
        $target_dir = "../images/";
        $image = basename($_FILES["image"]["name"]);
        $target_file = $target_dir . $image;
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
    }
    $post->img = $image;
        $post->create();
        header('Location: ../index.php');
    }
}


?>

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
            <p class="pull-right visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
            </p>
            <div class="jumbotron">
                <h1>Add New Post</h1>
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" class="form-control" placeholder="Title" required>
                    </div>
                    <div class="form-group">
                        <label for="category_id">Title</label>
                        <select name="category_id" id="category_id" class="form-control">
                            <option disabled selected>Choose Category</option>
                            <?php foreach ($categories as $category) {
                                echo "<option value='".$category['id']."'>".$category['name']."</option>";
                            } ?>

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="img">Image</label>
                        <input type="file" name="image" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea name="content" id="content" cols="30" rows="10" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="create" class="btn btn-primary" value="Submit">
                    </div>
                </form>
            </div>
        </div><!--/.col-xs-12.col-sm-9-->


    </div><!--/row-->

    <hr>

    <footer>
        <p>&copy; <?php echo date('Y')?> Mousa, Inc.</p>
    </footer>

</div><!--/.container-->



<?php include '../partials/footer.inc'; ?>
