<?php include '../partials/header.inc';

require '../controllers/Post.php';
require '../controllers/Comment.php';
$post = new Post;
$myPost = $post->getPost($_GET['id']);

$comment = new Comment();
$comments = $comment->comments($_GET['id']);
if (isset($_POST['create'])) {
    $comment->content = $_POST['comment'];
    $comment->user_id = $_SESSION['user_id'];
    $comment->post_id = $_GET['id'];
    $comment->create();
    header('Location: view.php?id='.$_GET['id']);
}
?>

<div class="container">

    <div class="jumbotron">
        <?php if ($myPost[0]['user_id'] == $_SESSION['user_id']) { ?>
        <button class="btn btn-danger pull-right delete-post" style="margin-left: 5px;: " data-id="<?php echo $_GET['id']?>"><span class="fa fa-trash" aria-hidden="true"></span> Delete</button>
        <a href="edit.php?id=<?php echo $myPost[0]['id']?>" class="btn btn-primary pull-right"><span class="fa fa-pencil" aria-hidden="true"></span> Edit</a>
        <?php } ?>

        <h1><?php echo $myPost[0]['title'] ?></h1>
        <?php if (!empty($myPost[0]['img'])) { ?>
        <img src="../images/<?php echo $myPost[0]['img'] ?>" alt="Post image" class="img-responsive img-rounded">
        <?php } ?>
        <p class="lead">
            <?php echo $myPost[0]['content']?>
        </p>
    </div>

    <div class="row marketing">
        <div class="col-md-9">
            <h1>Comments</h1>
            <hr style="border-width: 3px;">
            <?php foreach ($comments as $comm) { ?>
                <div class="pull-right">
                    <?php if ($comm['user_id'] == $_SESSION['user_id']) { ?>
                    <a href="edit_comment.php?id=<?php echo $comm['id']?>&post_id=<?php echo $comm['post_id']?>" class="btn btn-primary"><span class="fa fa-pencil"></span></a>
                    <a href="delete_comment.php?id=<?php echo $comm['id']?>&post_id=<?php echo $comm['post_id']?>" class="btn btn-danger"><span class="fa fa-trash"></span></a>
                    <?php } ?>
                </div>
                <p><?php echo $comm['content']?></p>
                <h5><i>By: </i><?php echo $comment->getUsername($comm['user_id']) ?></h5>
                <hr>
            <?php } ?>
        </div>
    </div>
    <div class="row marketing">
        <div class="col-md-9">
            <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$_GET['id'] ?>" method="post">
                <div class="form-group">
                    <input type="text" name="comment" class="form-control" placeholder="Write a comment ..." required>
                    <input type="submit" name="create" value="Add" class="btn btn-primary pull-right" style="margin-top: 5px">
                </div>
            </form>
        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; <?php echo date('Y')?> Mousa, Inc.</p>
    </footer>

</div><!--/.container-->





<?php include '../partials/footer.inc'; ?>

