

<?php include 'partials/header.inc';
require '../controllers/Setting.php';
$setting = new Setting();
$about = $setting->getAbout();
$contact = $setting->getContact();
if (isset($_POST['updateAbout'])) {
    $setting->key = 'about';
    $setting->value = $_POST['about_content'];
    $setting->update();
    header('Location: settings.php');
}
if (isset($_POST['updateContact'])) {
    $setting->key = 'contact';
    $setting->value = $_POST['contact_content'];
    $setting->update();
    header('Location: settings.php');
}

?>

<div id="wrapper">

    <!-- Navigation -->

    <!--nav-->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header"> <?php echo strtoupper("welcome"." ".htmlentities($_SESSION['user_name']));?></h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Settings</div>
                    <div class="panel-body">
                        <div class="row">
                            <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>">
                                <div class="col-lg-10">


                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>About<span id="" style="font-size:11px;color:red">*</span>	</label>
                                        </div>
                                        <div class="col-lg-6">

                                            <textarea name="about_content" class="form-control" cols="30" rows="10"><?php echo $about?></textarea>
                                            <span id="course-availability-status" style="font-size:12px;"></span>
                                        </div>

                                    </div>

                                    <br><br>
                                    <div class="form-group">
                                        <div class="col-lg-4">

                                        </div>
                                        <div class="col-lg-6"><br><br>
                                            <input type="submit" class="btn btn-primary" name="updateAbout" value="Update" style="margin-bottom: 20px">
                                        </div>

                                    </div>
                                </form>
                            <br><br>
                            <br><br>
                            <br><br>


                            <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>">

                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Contact<span id="" style="font-size:11px;color:red">*</span></label>
                                        </div>
                                        <div class="col-lg-6">
                                            <textarea name="contact_content" id="content" placeholder="Type a content" cols="30" rows="10" class="form-control" ><?php echo $contact?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <br><br>

                                <div class="form-group">
                                    <div class="col-lg-4">

                                    </div>
                                    <div class="col-lg-6"><br><br>
                                        <input type="submit" class="btn btn-primary" name="updateContact" value="Update">
                                    </div>

                                </div>
                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<?php include 'partials/footer.inc'?>