<?php
//session_start();
$url = 'http://'.$_SERVER['HTTP_HOST'].'/blog';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog Control</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $url?>/css/bootstrap.min.css"
          rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo $url?>/css/metisMenu.min.css"
          rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo $url?>/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo $url?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php include('navbar.php')?>