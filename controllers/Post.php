<?php
session_start();
require_once 'DBConnect.php';
/**
 * Created by PhpStorm.
 * User: mousa
 * Date: 25/05/19
 * Time: 02:56 ص
 */
class Post
{
    public $title;
    public $content;
    public $img;
    public $user_id;
    public $category_id;
    public $fav = 0;
    private $db;

    public function __construct()
    {
        $this->db = new DBConnect();
    }

    public function index()
    {
        return $this->db->selectAll('SELECT * FROM posts WHERE fav=0 ORDER BY created_at DESC limit 9');
    }

    public function getAllPosts()
    {
        return $this->db->selectAll('SELECT * FROM posts ORDER BY created_at DESC limit 9');
    }
    public function create()
    {
        $this->db->insertDB('INSERT INTO posts (title, img, content, user_id, category_id, fav) VALUES (?,?,?,?,?,?)', [
            $this->title, $this->img, $this->content, $this->user_id, $this->category_id, $this->fav
        ]);
    }

    public function getAllCats()
    {
        return $this->db->selectAll('SELECT * FROM categories');
    }

    public function getMyPosts()
    {
        return $this->db->selectDB('SELECT * FROM posts WHERE user_id=? ORDER BY created_at DESC limit 9', [$_SESSION['user_id']]);
    }

    public function favPosts()
    {
        return $this->db->selectAll('SELECT * FROM posts WHERE fav=1 ORDER BY created_at DESC limit 1');
    }

    public function getPost($id)
    {
        return $this->db->selectDB('SELECT * FROM posts WHERE id=?', [$id]);
    }

    public function update($id)
    {
        $this->db->insertDB('UPDATE posts SET title=?,category_id=?,content=?,img=? WHERE id=?', [$this->title, $this->category_id, $this->content, $this->img, $id]);
    }

    public function delete($id)
    {
        $this->db->deleteRow('DELETE FROM posts WHERE id=?', [$id]);
    }

    public function postUser($user_id)
    {
        return $this->db->selectDB('SELECT * FROM users WHERE id=?', [$user_id]);
    }

    public function postCategory($category_id)
    {
        return $this->db->selectDB('SELECT * FROM categories WHERE id=?', [$category_id]);
    }
}