<nav class="navbar navbar-default navbar-static-top" role="navigation"
     style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span> <span
                class="icon-bar"></span> <span class="icon-bar"></span> <span
                class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo $url?>/admin">Blog Dashboard</a>
    </div>
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">

                <li>
                    <a href="<?php echo $url ?>/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-users fa-fw"></i> Users<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo $url?>/admin/users/create.php">Add User</a>
                        </li>
                        <li>
                            <a href="<?php echo $url?>/admin/users/view.php">View</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-caret-down fa-fw"></i> Categories<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo $url?>/admin/categories/create.php">Add Category</a>
                        </li>
                        <li>
                            <a href="<?php echo $url?>/admin/categories/view.php">View</a>
                        </li>
                    </ul>

                </li>

                <li>
                    <a href="<?php echo $url?>/admin/posts/view.php"><i class="fa fa-edit fa-fw"></i> Posts</a>
                </li>

                <li>
                    <a href="<?php echo $url?>/admin/settings.php"><i class="fa fa-wrench fa-fw"></i> Setting </a>
                </li>
                <li>
                    <a href="<?php echo $url?>/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout </a>
                </li>
            </ul>
        </div>

    </div>

</nav>