<?php
include '../partials/header.inc';
require '../../controllers/Category.php';
$category = new Category;
if (isset($_POST['create'])) {

    $name = $_POST['name'];

    $category->name = $name;
    $category->create();
    header('location: view.php');
}
?>

<div id="wrapper">

    <!-- Navigation -->

    <!--nav-->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header"> <?php echo strtoupper("welcome"." ".htmlentities($_SESSION['user_name']));?></h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Category</div>
                    <div class="panel-body">
                        <div class="row">
                            <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>">
                                <div class="col-lg-10">


                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Name<span id="" style="font-size:11px;color:red">*</span>	</label>
                                        </div>
                                        <div class="col-lg-6">

                                            <input class="form-control" name="name" placeholder="Name .." id="cshort" required="required"  onblur="courseAvailability()">
                                            <span id="course-availability-status" style="font-size:12px;"></span>				</div>

                                    </div>

                                    <br><br>

                                </div>

                                <br><br>

                                <div class="form-group">
                                    <div class="col-lg-4">

                                    </div>
                                    <div class="col-lg-6"><br><br>
                                        <input type="submit" class="btn btn-primary" name="create" value="Create Category">
                                    </div>

                                </div>
                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<?php include '../partials/footer.inc'?>
