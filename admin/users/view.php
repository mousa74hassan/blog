<?php
include '../partials/header.inc';
require '../../controllers/User.php';
$user = new User;
$users = $user->index();
?>

<div id="wrapper">

    <!-- Navigation -->

    <!--nav-->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header"> <?php echo strtoupper("welcome"." ".htmlentities($_SESSION['user_name']));?></h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Users</div>
                    <div class="panel-body">
                        <div class="row">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Actions</th>
                                </thead>
                                <tbody>
                                <?php foreach($users as $user) { ?>
                                <tr>
                                    <td><?php echo $user['id'] ?></td>
                                    <td><?php echo $user['name'] ?></td>
                                    <td><?php echo $user['email'] ?></td>
                                    <td><?php echo $user['status'] == 1 ? 'Admin' : 'User' ?></td>
                                    <td><a href="edit.php?id=<?php echo $user['id']?>" class="btn btn-default">Edit</a> <a href="delete.php?id=<?php echo $user['id']?>" class="btn btn-danger">Delete</a></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<?php include '../partials/footer.inc'?>
