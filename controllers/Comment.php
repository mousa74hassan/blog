<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 27/05/2019
 * Time: 11:39 AM
 */

class Comment
{
    public $content;
    public $user_id;
    public $post_id;
    private $db;

    public function __construct()
    {
        $this->db = new DBConnect();
    }

    public function create()
    {
        $this->db->insertDB('INSERT INTO comments (content, user_id, post_id) VALUES (?,?,?)', [
            $this->content, $this->user_id, $this->post_id
        ]);
    }

    public function update($id)
    {
        $this->db->insertDB('UPDATE comments SET content=? WHERE id=?', [$this->content, $id]);
    }

    public function getComment($id)
    {
        return $this->db->selectDB('SELECT content FROM comments WHERE id = ?', [$id]);
    }

    public function delete($id)
    {
        $this->db->deleteRow('DELETE FROM comments WHERE id=?', [$id]);
    }

    public function comments($post_id)
    {
        return $this->db->selectDB('SELECT * FROM comments WHERE  post_id=?', [$post_id]);
    }

    public function getUsername($id)
    {
        return ($this->db->selectDB('SELECT name FROM users WHERE id=?', [$id]))[0]['name'];
    }

}