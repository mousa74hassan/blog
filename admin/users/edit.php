<?php
include '../partials/header.inc';
require '../../controllers/User.php';
$user = new User;
$curr_user = $user->getUser($_GET['id']);
if (isset($_POST['update'])) {

    $name = $_POST['name'];
    $password = $_POST['password'];
    $email = $_POST['email'];
    $status = $_POST['status'];

    $user->name = $name;
    $user->email = $email;
    if (empty($password)) {
        $user->password = $curr_user[0]['password'];
    } else {
        $user->password = md5($password);
    }
    $user->status = $status;
    $user->update($_GET['id']);
    header('location: view.php');
}
?>

<div id="wrapper">

    <!-- Navigation -->

    <!--nav-->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header"> <?php echo strtoupper("welcome"." ".htmlentities($_SESSION['user_name']));?></h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit User</div>
                    <div class="panel-body">
                        <div class="row">
                            <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$_GET['id'] ?>">
                                <div class="col-lg-10">


                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Name<span id="" style="font-size:11px;color:red">*</span>	</label>
                                        </div>
                                        <div class="col-lg-6">

                                            <input class="form-control" name="name" value="<?php echo $curr_user[0]['name'] ?>" placeholder="Title .." id="cshort" required="required"  onblur="courseAvailability()">
                                            <span id="course-availability-status" style="font-size:12px;"></span>				</div>

                                    </div>

                                    <br><br>

                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Email<span id="" style="font-size:11px;color:red">*</span>	</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input class="form-control" type="email" value="<?php echo $curr_user[0]['email'] ?>" name="email" placeholder="Title .." id="email" required="required"  onblur="courseAvailability()">
                                            <span id="course-availability-status" style="font-size:12px;"></span>				</div>

                                    </div>

                                    <br><br>

                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Password</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="password" class="form-control" name="password" id="password">
                                            <span id="course-status" style="font-size:12px;"></span>				</div>
                                    </div>

                                    <br><br>

                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Type<span id="" style="font-size:11px;color:red">*</span></label>
                                        </div>
                                        <div class="col-lg-6">
                                            <label><input type="radio" name="status" value="1" <?php echo $curr_user[0]['status'] == 1 ? 'checked' : '' ?> > Admin </label>
                                            <label><input type="radio" name="status" value="0" <?php echo $curr_user[0]['status'] == 0 ? 'checked' : '' ?> > User </label>
                                        </div>
                                    </div>
                                </div>

                                <br><br>

                                <div class="form-group">
                                    <div class="col-lg-4">

                                    </div>
                                    <div class="col-lg-6"><br><br>
                                        <input type="submit" class="btn btn-primary" name="update" value="Create User">
                                    </div>

                                </div>
                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<?php include '../partials/footer.inc'?>
