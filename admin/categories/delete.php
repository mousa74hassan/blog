<?php
/**
 * Created by PhpStorm.
 * User: mousa
 * Date: 30/05/19
 * Time: 05:55 م
 */

require '../../controllers/Category.php';
$url = 'http://'.$_SERVER['HTTP_HOST'].'/blog';

$category = new Category;
if (isset($_GET['type'])) {
    $category->delete($_GET['id']);
    header("Location: view.php");
}
?>
<script>
    if (confirm('Are you sure?')) {
        window.location.href = "<?php echo $url.'/admin/categories/delete.php?id='.$_GET['id'].'&type=sure'?>"
    } else {
        window.location.href = "<?php echo $url.'/admin/categories/view.php'?>"
    }
</script>
