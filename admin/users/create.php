<?php
include '../partials/header.inc';
require '../../controllers/User.php';
$user = new User;
if (isset($_POST['create'])) {

    $name = $_POST['name'];
    $password = $_POST['password'];
    $email = $_POST['email'];
    $status = $_POST['status'];

    $user->name = $name;
    $user->email = $email;
    $user->password = md5($password);
    $user->status = $status;
    $user->create();
    header('location: view.php');
}
?>

<div id="wrapper">

    <!-- Navigation -->

    <!--nav-->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header"> <?php echo strtoupper("welcome"." ".htmlentities($_SESSION['user_name']));?></h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Add User</div>
                    <div class="panel-body">
                        <div class="row">
                            <form method="post" >
                                <div class="col-lg-10">


                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Name<span id="" style="font-size:11px;color:red">*</span>	</label>
                                        </div>
                                        <div class="col-lg-6">

                                            <input class="form-control" name="name" placeholder="Name .." id="cshort" required="required"  onblur="courseAvailability()">
                                            <span id="course-availability-status" style="font-size:12px;"></span>				</div>

                                    </div>

                                    <br><br>

                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Email<span id="" style="font-size:11px;color:red">*</span>	</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input class="form-control" type="email" name="email" placeholder="example@example.com" id="email" required="required"  onblur="courseAvailability()">
                                            <span id="course-availability-status" style="font-size:12px;"></span>				</div>

                                    </div>

                                    <br><br>

                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Password<span id="" style="font-size:11px;color:red">*</span></label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="password" class="form-control" name="password" id="password" onblur="coursefullAvail()">
                                            <span id="course-status" style="font-size:12px;"></span>				</div>
                                    </div>

                                    <br><br>

                                    <div class="form-group">
                                        <div class="col-lg-4">
                                            <label>Type<span id="" style="font-size:11px;color:red">*</span></label>
                                        </div>
                                        <div class="col-lg-6">
                                            <label><input type="radio" name="status" value="1"> Admin </label> <br>
                                            <label><input type="radio" name="status" value="0"> User </label>
                                        </div>
                                    </div>
                                </div>

                                <br><br>

                                <div class="form-group">
                                    <div class="col-lg-4">

                                    </div>
                                    <div class="col-lg-6"><br><br>
                                        <input type="submit" class="btn btn-primary" name="create" value="Create User">
                                    </div>

                                </div>
                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<?php include '../partials/footer.inc'?>
