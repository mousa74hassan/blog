<?php
session_start();
require_once 'DBConnect.php';

/**
 * Created by PhpStorm.
 * User: mousa
 * Date: 30/05/19
 * Time: 07:29 م
 */
class Category
{
    public $name;
    private $db;

    public function __construct()
    {
        $this->db = new DBConnect();
    }

    public function index()
    {
        return $this->db->selectAll('SELECT * FROM categories');
    }

    public function create()
    {

        $this->db->insertDB('INSERT INTO categories (name) VALUES (?)', [$this->name]);
    }

    public function postsOfCategory($id)
    {
        return $this->db->selectDB('SELECT * FROM posts WHERE category_id=?', [$id]);
    }

    public function getCategory($id)
    {
        return $this->db->selectDB('SELECT * FROM categories WHERE id=?', [$id]);
    }

    public function update($id)
    {
        $this->db->insertDB('UPDATE categories SET name=?WHERE id=?', [$this->name, $id]);
    }

    public function delete($id)
    {
        $this->db->deleteRow('DELETE FROM categories WHERE id=?', [$id]);
    }
}