<?php

/**
 * Created by PhpStorm.
 * User: mousa
 * Date: 24/05/19
 * Time: 03:22 م
 */
class DBConnect
{
    private $host = 'localhost';
    private $dbName = 'blog';
    private $dbUserName = 'root';
    private $dbPass = '';
    private $connection;

    public function __construct()
    {
        try {
            $this->connection = new PDO("mysql:host=$this->host;dbname=$this->dbName", $this->dbUserName, $this->dbPass);
        } catch (PDOException $e) {
            die('Connection Failed: '. $e->getMessage());
        }
    }

    public function insertDB($stat, $data)
    {
        $this->connection->prepare($stat)->execute($data);
    }

    public function selectDB($stat, $data)
    {
        $st = $this->connection->prepare($stat);
        $st->execute($data);
        return $st->fetchAll(PDO::FETCH_ASSOC);
    }

    public function selectAll($statment)
    {
        return $this->connection->query($statment)->fetchAll();
    }

    public function deleteRow($stat,$data)
    {
        $this->connection->prepare($stat)->execute($data);
    }

    public function __destruct()
    {
        unset($this->connection);
    }
}