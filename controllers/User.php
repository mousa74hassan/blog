<?php
session_start();
require_once 'DBConnect.php';

/**
 * Created by PhpStorm.
 * User: mousa
 * Date: 24/05/19
 * Time: 02:49 م
 */
class User
{
    public $name;
    public $email;
    public $password;
    public $status;
    private $db;

    public function __construct()
    {
        $this->db = new DBConnect();
    }

    public function index()
    {
        return $this->db->selectAll('SELECT * FROM users');
    }

    public function create()
    {

        $this->db->insertDB('INSERT INTO users (name, email, password, status) VALUES (?, ?, ?, ?)', [$this->name, $this->email, $this->password, $this->status]);
    }

    public function chkLogin ($email, $pass)
    {
        $q = $this->db->selectDB('SELECT * FROM users WHERE email=? AND password=?', [$email, $pass]);
        if (count($q) > 0) {
            $_SESSION['user_name'] = $q[0]['name'];
            $_SESSION['user_email'] = $q[0]['email'];
            $_SESSION['user_id'] = $q[0]['id'];
            $_SESSION['user_created_at'] = $q[0]['created_at'];
            return true;
        }
        return false;
    }

    public function ckAdmin ($email, $pass)
    {
        $q = $this->db->selectDB('SELECT * FROM users WHERE email=? AND password=? AND status=1', [$email, $pass]);
        if (count($q)) {
            $_SESSION['user_name'] = $q[0]['name'];
            $_SESSION['user_email'] = $q[0]['email'];
            $_SESSION['user_id'] = $q[0]['id'];
            $_SESSION['user_created_at'] = $q[0]['created_at'];
            return true;
        }
        return false;
    }

    public function getUser($id)
    {
        return $this->db->selectDB('SELECT * FROM users WHERE id=?', [$id]);
    }

    public function update($id)
    {
        $this->db->insertDB('UPDATE users SET name=?,email=?,password=?,status=? WHERE id=?', [$this->name, $this->email, $this->password, $this->status, $id]);
    }

    public function delete($id)
    {
        $this->db->deleteRow('DELETE FROM users WHERE id=?', [$id]);
    }

}

